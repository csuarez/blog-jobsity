<?php

use Illuminate\Database\Seeder;

class usersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
        DB::table("users")->truncate();


        //Create Players
        factory(App\User::class, 20)->create(
            [
            'password' => bcrypt('123456')
            ]
        );
        //My Own Users
        factory(App\User::class, 1)->create(
            [
                'username' => 'cesarlarsson',
                'email'=>'cesarlarsson@gmail.com',
                'password' => bcrypt('123456'),
                'twitteruser'=>'cesarlarsson'
            ]
        );
        factory(App\User::class, 1)->create(
            [
                'username' => 'cesarsuarez',
                'email'=>'cesarn@smartrabbit.co',
                'password' => bcrypt('123456'),
                'twitteruser'=>'phaserjs'
            ]
        );
        $users = App\User::all();
        foreach ($users as $user) {

            factory(App\Entry::class)->create([
                'user_id' => $user->id
            ]);

            factory(App\Entry::class)->create([
                'user_id' => $user->id
            ]);
            factory(App\Entry::class)->create([
                'user_id' => $user->id
            ]);

        }



        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
