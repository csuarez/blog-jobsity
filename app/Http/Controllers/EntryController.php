<?php

namespace App\Http\Controllers;

use App\Entry;
use App\tweets_hide;
use App\User;
use App\Exceptions\Handler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Thujohn\Twitter\Facades\Twitter;



class EntryController extends Controller
{
    /**
     * Display the home with the last post
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Get the las entries from all users
        $entries = Entry::filterAndPaginate();

        foreach($entries as $entry){
            $entry->content=$this->limit_words($entry->content, 100);
        }

        return view('entries.index',compact('entries'));
    }


    private function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('entries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required'
        ]);


        if ($validator->fails()) {

            return redirect('createnew')
                ->withErrors($validator)
                ->withInput();
        }


        $entry = new Entry();
        $entry->title = $request->get('title');
        $entry->content = $request->get('content');
        $entry->user_id =Auth::user()->id;

        if($entry->save()){

            Session::flash('message', 'Entry successfully created');
        }else{
            Session::flash('alert-class', 'Error created the entry');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        //
        try {
            $entry = Entry::leftJoin('users', 'users.id', '=', 'entries.user_id')->where('entries.id',$id)->firstOrFail();

            return view('entries.view', compact('entry'));


        }catch (ModelNotFoundException $e){

            return redirect()->back();
        }

    }

    /**
     * Get all the entries of an user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewuserentries($iduser=null){



        try {
            $userdata=User::findOrFail($iduser);

            $entries =   Entry::leftJoin('users', 'users.id', '=', 'entries.user_id')
                ->select('users.username','users.id as userid','entries.*')
                ->where('users.id',$iduser)->paginate(3);

            foreach($entries as $entry){
                $entry->content=$this->limit_words($entry->content, 100)." ...";
            }

            $authcheck=null;
            //Get the las 20 tweet of the user
            try {
                // $tweets = Twitter::getUserTimeline(['screen_name' =>$userdata->twitteruser , 'count' => 2, 'format' => 'array','since_id'=>'671347566051778560']);
                // $tweets = Twitter::getUserTimeline(['screen_name' =>$userdata->twitteruser , 'count' => 5, 'format' => 'array','max_id'=>'671347566051778560']);
                $tweets = Twitter::getUserTimeline(['screen_name' => $userdata->twitteruser, 'count' => 5, 'format' => 'array']);
                //Check if the tweet is hide
                $i = 0;
                foreach ($tweets as $tweet) {
                    $element = tweets_hide::where('tweet_id', $tweet['id_str'])->first();
                    if ($element != null) {
                        $tweets[$i]['visible'] = false;
                    } else {
                        $tweets[$i]['visible'] = true;
                    }

                    $i++;

                }


                if (Auth::check()){
                    $usertitter = Auth::user()->twitteruser;
                    if($usertitter==$userdata->twitteruser){
                        $authcheck=true;
                    }
                }


            }catch (\Exception $e){
              // dd($e->getMessage());
                $tweets=null;

            }

            return view('entries.userentries', compact('entries','userdata','tweets','authcheck'));
        }catch (ModelNotFoundException $e){

            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {
            $entry = Entry::findOrFail($id);
            return view('entries.edit', compact('entry'));
        }catch (ModelNotFoundException $e){

            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required'
        ]);


        if ($validator->fails()) {

            return redirect('editentry/'.$id)
                ->withErrors($validator)
                ->withInput();
        }
        $this->entry = Entry::findOrFail($id);
        $this->entry->fill($request->all());

        if($this->entry->save()){

            Session::flash('message', 'Entry successfully updated');
        }else{
            Session::flash('alert-class', 'Error updating the entry');
        }

        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the tweets from a user until an specific id
     *
     * @param  int  $iduser
     * @param  int  $idtweet
     * @return \Illuminate\Http\Response
     */
    public function getmoretweets(Request $request){

             if($request->ajax()){
                $iduser=$request->datos['id'];
                $idlasttweet=$request->datos['idtweet'];
                 //Get the last 6 tweet of the user before the tweet id given
                 try {
                     $tweets = Twitter::getUserTimeline(['screen_name' =>$iduser, 'count' => 6, 'format' => 'array','max_id'=>$idlasttweet]);

                    $i=0;

                     foreach($tweets as $tweet){

                         //$tweet['html']=Twitter::linkify($tweet['text']);
                         $tweets[$i]['text']=Twitter::linkify($tweet['text']);
                         $tweets[$i]['created_at']=Twitter::ago($tweet['created_at']);

                         //Check if the tweet is hide
                         $element=tweets_hide::where('tweet_id',"=",$tweet['id_str'])->first();

                         if($element!=null){
                             $tweets[$i]['visible']=false;

                         }else{
                             $tweets[$i]['visible']=true;
                         }
                         $i++;


                     }
                     $authcheck=null;
                     unset($tweets[0]);
                     if (Auth::check()){
                         $usertitter = Auth::user()->twitteruser;
                         if($usertitter==$iduser){
                             $authcheck=true;
                         }
                     }


                 }catch (\Exception $e){
                     $tweets=null;


                 }

               //  return "true";
                 return Response::json(['data' => $tweets]);

             }
        return null;
    }


    public function hidetweet(Request $request){

        if($request->ajax()){
            try {
                $tweet_id=$request->datos['idtweet'];
                $tweet_str=$request->datos['tweet'];
                if(isset($request->datos['date'])) {
                    $tweet_date = $request->datos['date'];
                }
                $authcheck=null;
                //check if the tweet already in the hidden list
                $tweet_info=tweets_hide::where('tweet_id', $tweet_id)->first();
                if($tweet_info){


                    if (Auth::check()){
                        $usertitter = Auth::user()->twitteruser;
                        if($usertitter==$tweet_info->twitteruser){
                            $authcheck=true;
                        }
                    }
                    if(tweets_hide::where('tweet_id', $tweet_id)->delete()){

                        return Response::json(['data' => true,'hide'=>false,'authcheck'=>$authcheck,'tweet_data'=>$tweet_info]);
                    }else{
                        return Response::json(['data' => false,'hide'=>false,'authcheck'=>$authcheck,'tweet_data'=>$tweet_info]);
                    }
                }else {

                    $iduser = Auth::user()->twitteruser;
                    $idlasttweet = $tweet_id;
                    $tweet_hide = new tweets_hide();
                    $tweet_hide->tweet_id = $idlasttweet;
                    $tweet_hide->twitteruser = $iduser;
                    $tweet_hide->tweet = $tweet_str;
                    $tweet_hide->date = $tweet_date;

                    if ($tweet_hide->save()) {
                        return Response::json(['data' => true,'authcheck'=>true,'hide'=>true,'tweet_data'=>$tweet_hide]);

                    }
                }

                return Response::json(['data' => false]);
            }catch (QueryException $e){
                return Response::json(['data' => false,'error'=>$e->getMessage()]);
            }catch(\Exception $e){
                return Response::json(['data' => false,'error'=>$e->getMessage()]);
            }
        }
        return null;
    }


}
