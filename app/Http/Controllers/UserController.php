<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the basic information for all the users

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'email' => 'required',
            'repeatpassword'=>'required|min:5|max:30|same:password',
            'twitteruser' => 'unique:users,twitteruser',
        ]);


        if ($validator->fails()) {

            return redirect('register')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $user = new User;
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->twitteruser = $request->get('twitteruser');
            $user->password =bcrypt($request->get('password'));
            if($user->save()){
                Session::flash('message', 'User created');
            }
        }catch (QueryException $e){

            Session::flash('message', 'Error creating the user ');

        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the the current user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {

        try {
            $user = User::findOrFail(Auth::user()->id);
            return view('user.profile', compact('user'));
        }catch (ModelNotFoundException $e){

            return redirect()->back();
        }

    }
}
