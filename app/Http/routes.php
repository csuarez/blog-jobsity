<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


//Home
Route::get('/', [
    'uses' => 'EntryController@index',
    'as'=>'home'
]);
Route::get('home', [
    'uses' => 'EntryController@index',
    'as'=>'home'
]);

//Login
Route::get('login',['middleware' => 'guest',
    'as' => 'login',
    function () {
    return view('auth/login');
}]);

Route::post('login', 'Auth\AuthController@postLogin');


//Register User
Route::get('register',
    ['middleware' => 'guest',
        'as' => 'register',
        'uses' => 'UserController@create']);
Route::post('register',
    ['middleware' => 'guest',
        'as' => 'register',
        'uses' => 'UserController@store']);

//Salir
Route::get('logout',[
    'uses'=>'Auth\AuthController@getLogout',
    'as'=>'logout'
]);

//Recover Password
Route::get('recover_password',[ function () {
    return view('auth/recover');
}]);
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('password/reset/{token?}', 'Auth\PasswordController@getReset');
Route::post('password/reset/{token?}', 'Auth\PasswordController@postReset');

//View Entry
Route::get('viewentry/{id?}',[
    'uses'=>'EntryController@Show',
    'as'=>'viewentry'
]);


//Create new entry
Route::get('createnew',[
    'middleware' => 'auth',
    'uses'=>'EntryController@Create',
    'as'=>'createnew'
]);
Route::post('createnew',[
    'middleware' => 'auth',
    'uses'=>'EntryController@Store',
    'as'=>'createnew'
]);

//View entries from a user
Route::get('viewuserentries/{iduser?}',[

    'uses'=>'EntryController@viewuserentries',
    'as'=>'viewuserentries'
]);


//Edit Post
Route::get('editentry/{id?}',[
    'middleware' => 'auth',
    'uses'=>'EntryController@Edit',
    'as'=>'editentry'
]);

Route::group(['middleware' => ['auth']], function () {
    Route::resource('entry', 'EntryController');
});

//User profile page
Route::get('profile',[
    'uses'=>'UserController@profile',
    'as'=>'profile'
]);

//Get more tweets for an user only with ajax
Route::post('/getmoretweets',[
    'uses'=>'EntryController@getmoretweets',
    'as'=>'getmoretweets'
]);
//Hide a tweet from the user
Route::post('/hidetweet',[
    'uses'=>'EntryController@hidetweet',
    'as'=>'hidetweet'
]);

