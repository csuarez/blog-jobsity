<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'entries';

    protected $fillable = ['title', 'content'];

    // Queries
    public static function filterAndPaginate()
    {
        return Entry::leftJoin('users', 'users.id', '=', 'entries.user_id')->orderBy('entries.created_at','DESC')->select('users.username','users.id as userid','entries.*')->
            paginate(3);

    }
}
