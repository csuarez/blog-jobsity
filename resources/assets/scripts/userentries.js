/**
 * Created by cesarsuarez on 12/11/15.
 */
$(document).ready(function(){
    var source   = $("#entry-tweet-template").html();
    var source2 =$("#entry-empty-tweet-template").html();
    var template = Handlebars.compile(source);
    var template2 = Handlebars.compile(source2);
    $(document).on("click",".btn-hide-tweet",function(){
        var idlastweet=$(this).parent().data("idtweet");
        var tweet=$(this).parent().find("p").html();
        var date=$(this).parent().data("date");
        var divelement=$(this);
        jQuery.ajax({
                type: "POST",
                url: '/hidetweet',
                data: {
                    datos: {idtweet:idlastweet,tweet:tweet,date:date,iduser:'{{ $userdata->twitteruser }}'}
                },
                success: function (data) {
                    if(data.data==true){
                        if(data.hide==true) {
                            var context = {
                                text: data.tweet_data.tweet,
                                idtweet: data.tweet_data.tweet_id,
                                date: data.tweet_data.date,
                                created_at: data.tweet_data.created_at,
                                auth: data.authcheck
                            };
                            var html = template2(context);
                            $(html).insertAfter($(divelement).parent());
                        }else{
                            var context = {
                                text: data.tweet_data.tweet,
                                idtweet: data.tweet_data.tweet_id,
                                date: data.tweet_data.date,
                                created_at: data.tweet_data.created_at,
                                auth: data.authcheck
                            };
                            var html = template(context);
                            $(html).insertAfter($(divelement).parent());
                        }
                        $(divelement).parent().remove();
                        // $(this).parent().hide();
                    }
                }
            }
        );


    });

    $("#btn-load-more").on("click",function(){
        //Get the id pf the last tweet
        var idlastweet=$(".tweets .tweet-container").last().data("idtweet");
        $(".preloader").show();
        jQuery.ajax({
                type: "POST",
                url: '/getmoretweets',
                data: {
                    datos: {id:'{{ $userdata->twitteruser }}',idtweet:idlastweet.trim()}
                },
                success: function (data) {
                    $.each(data.data,function( index, value ) {
                        if(value.visible==true) {
                            var context = {
                                text: value.text,
                                idtweet: value.id_str,
                                date: value.created_at,
                                created_at: value.created_at,
                                auth: auth
                            };
                            var html = template(context);
                        }else{
                            var context = {
                                text: value.text,
                                idtweet: value.id_str,
                                date: value.created_at,
                                created_at: value.created_at,
                                auth: auth
                            };
                            var html = template2(context);
                        }
                        $(".tweets .tweets-container").append(html);
                    });
                    $(".preloader").hide();
                }
            }
        );
    });
});