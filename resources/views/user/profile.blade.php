@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Profile</h1>
        </div>
    </header>
    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">

                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="bs-example" data-example-id="condensed-table">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr> <th scope="row">Username</th> <td>{{  Auth::user()->username }}</td>  </tr>
                                    <tr> <th scope="row">Email</th> <td>{{  Auth::user()->email }}</td>  </tr>
                                    <tr> <th scope="row">TwitterUser</th> <td colspan="2">{{ '@'. Auth::user()->twitteruser }}</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div><!--/col-12-->
        </div>
    </div>


    @endsection