<script id="entry-tweet-template" type="text/x-handlebars-template">
    <div class="tweet-container" data-idtweet="{{idtweet}}" data-date="{{ created_at }}">
        <p>
             {{{text}}}
        </p>
        <br>
        {{date}}
        {{#with auth}}
            <button class="btn-sm btn-info btn-hide-tweet" >Hide Tweet</button>
        {{/with}}
    </div>
</script>

<script id="entry-empty-tweet-template" type="text/x-handlebars-template">
    <div class="tweet-container" data-idtweet="{{idtweet}}" data-date="{{ created_at }}">
      <p>The Owner hide this tweet</p>

        {{#with auth}}
      <button class="btn-sm btn-info btn-hide-tweet">Show Tweet</button>
        {{/with}}
    </div>
</script>