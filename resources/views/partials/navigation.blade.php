        <!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="/">BLOG</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                @if (!Auth::guest())
                <li>
                    <a class="page-scroll" href="{{ route("viewuserentries",['id'=>Auth::user()->id]) }}">Get My Entries</a>
                </li>
                <li>
                    <a class="page-scroll" href="/createnew">Add new Entry</a>
                </li>
                @endif
                @if (Auth::guest())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Log In<span class="caret"></span></a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form" role="form" method="post" action="{{ route('login')}}" accept-charset="UTF-8" id="login-nav">

                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputEmail2">Username</label>
                                            {!! Form::text('username', null, ['class' => 'form-control', 'type' => 'text','placeholder'=>"Username",'required'=>'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                            {!! Form::password('password', ['class' => 'form-control',"placeholder"=>"Password"]) !!}
                                            <div class="help-block text-right"><a href="/recover_password">Forgot Your Password?</a></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">Remember Me
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <div class="bottom text-center btn-registro ">
                                   <a href="/register"><b>Sign Up</b></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->username }} <span class="caret"></span></a>
                        <ul class="dropdown-menu submenu" role="menu">
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="{{ route('logout')}}">Logout</a></li>
                        </ul>
                    </li>

                @endif


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>