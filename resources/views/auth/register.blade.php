@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Sign Up</h1>
        </div>
    </header>
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                @include('partials.messages')

                {!! Form::open(['route' => 'register', 'method' => 'POST','class'=>'form-horizontal']) !!}

                <div class="row">

                    <div class="form-group form-group-lg">
                        {!! Form::label('username', 'Username',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">


                            {!! Form::text('username', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        {!! Form::label('twitteruser', null,['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">


                            {!! Form::text('twitteruser', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        {!! Form::label('email', 'Email',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-Mail']) !!}
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        {!! Form::label('password', 'Password',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        {!! Form::label('repeatpassword', 'Repeat Password',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::password('repeatpassword', ['class' => 'form-control']) !!}
                        </div>
                    </div>


                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-md-offset-6">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Sign Up</button></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

@endsection

@section('scripts')

@endsection