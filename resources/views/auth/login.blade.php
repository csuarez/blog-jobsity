@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Sign in</h1>
        </div>
    </header>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-6 col-md-offset-3">

                <div class="centering">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                Por favor corrige los siguientes errores:<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <div class="row">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login')}}">


                            <div class="form-group">
                                <label class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">
                                    {!! Form::text('username', null, ['class' => 'form-control', 'type' => 'email']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                                        Login
                                    </button>

                                    <a href="/password/email">Forgot Your Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')

    <style>
    html {
        height: 100%;
        width: 100%;
        overflow: hidden;
        min-width: 100%;
        min-height: 100%;
    }

    body {
        height: 100%;
        width: 100%;
        padding: 0;
        margin: 0;
    }
    </style>

@endsection


