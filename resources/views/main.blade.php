
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blog</title>
    <!-- Latest compiled and minified CSS -->
    {!! Html::style('assets/css/app.min.css') !!}
    {!! Html::style('assets/css/blog.min.css') !!}
    {!! Html::style('assets/css/vendor/bootstrap3-wysihtml5.min.css') !!}

    @yield('styles')

            <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>



@yield('content')
@include('partials.footer')
        <!-- Scripts -->

{!!   HTML::script('assets/scripts/vendor/jquery.min.js') !!}
{!!   HTML::script('assets/scripts/vendor/bootstrap.min.js') !!}
{!!   HTML::script('assets/scripts/vendor/bootstrap3-wysihtml5.all.min.js') !!}
{!!   HTML::script('assets/scripts/vendor/handlebars.min.js') !!}

@yield('scripts')

@include('partials.navigation')

</body>
</html>
