@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Home</h1>
        </div>
    </header>
    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-body">
                        @foreach($entries as $entry)
                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-8 col-sm-offset-2">
                                        <h3> {{ $entry->title }}</h3>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{ route('editentry', ['id' => $entry->id]) }}" class="btn btn-warning">Editar</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                        <p> {{ strip_tags ($entry->content) }}</p>
                                        <p class="lead"><a href="{{ route('viewentry', ['id' => $entry->id]) }}" class="btn btn-default">Read More</a></p>
                                        <p class="pull-right">Author:  <a href="{{ route('viewuserentries', ['iduser' => $entry->userid]) }}" class="label label-default">{{ $entry->username }}</a></p>
                                        <ul class="list-inline"><li><a href="#">

                                                    <?php
                                                    $dt = \Carbon\Carbon::parse($entry->created_at);
                                                    echo \Carbon\Carbon::now()->diffForHumans($dt);

                                                    ?>

                                                </a></li><li><a href="#"></a></ul>
                                    </div>
                                    <div class="col-xs-3"></div>
                                </div>
                                <br><br>
                            </div>

                        </div>
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-sm-offset-2">
                                <hr>
                                </div>
                            </div>
                        @endforeach
                            {!! $entries->appends(Request::input())->render() !!}
                    </div>
                </div>



            </div><!--/col-12-->
        </div>
    </div>


@endsection