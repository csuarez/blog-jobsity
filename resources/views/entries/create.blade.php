@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Create a New Entry</h1>
        </div>
    </header>
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                @include('partials.messages')

                {!! Form::open(['route' => 'createnew', 'method' => 'POST','class'=>'form-horizontal']) !!}

                <div class="row">

                    <div class="form-group form-group-lg">
                        {!! Form::label('title', 'Title',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">

                            {!! Form::text('title',NULL, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        {!! Form::label('content', 'Content',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">

                            {!! Form::textarea('content',NULL, ['class' => 'form-control','id'=>'contentarea']) !!}
                        </div>
                    </div>

                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-md-offset-6">
                            <button type="submit" class="btn btn-success btn-block btn-lg">ADD</button></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>


@endsection
            @section('scripts')

                <script>
                    $(document).ready(function(){

                        $('#contentarea').wysihtml5();
                    });

                </script>
            @endsection

