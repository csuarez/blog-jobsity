@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Create a New Entry</h1>
        </div>
    </header>
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                @include('partials.messages')


                <div class="row">

                    <div class="form-group form-group-lg">


                        <div class="col-sm-9">

                          <h3>  {!! $entry->title !!}</h3>
                           Created: {!! $entry->created_at !!}<br>
                            Author:<strong>{!! $entry->username !!}</strong>
                        </div>
                        <div class="col-sm-3">
                            <a href="{{ route('editentry', ['id' => $entry->id]) }}" class="btn btn-warning">Editar</a>
                        </div>

                    </div>

                    <div class="form-group form-group-lg">

                        <div class="col-sm-12">
                            <hr class="colorgraph">
                            <p>
                                {!! $entry->content !!}
                            </p>

                        </div>
                    </div>



                </div>
            </div>


            @endsection


