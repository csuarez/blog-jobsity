@extends('main')

@section('content')

    <header id="top" class="custom-page-header">
        <div class="container">

            <h1>Entries from {{ $userdata->username }}</h1>
        </div>
    </header>
    <div class="container user-entries">

        <div class="row">

            <div class="col-sm-8">

                <div class="panel">
                    <div class="panel-body">
                        @foreach($entries as $entry)


                            <div class="row">
                                <br>
                                <div class="col-sm-12">

                                    <div class="row">
                                        <div class="col-xs-8 col-sm-8 col-sm-offset-2">
                                    <h3> {{ $entry->title }}</h3>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="{{ route('editentry', ['id' => $entry->id]) }}" class="btn btn-warning">Editar</a>
                                        </div>
                                    </div>
                                        <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                            <p> {!! strip_tags($entry->content) !!}</p>
                                            <p class="lead"><a href="{{ route('viewentry', ['id' => $entry->id]) }}" class="btn btn-default">Read More</a></p>
                                            <p class="pull-right">Author:  <a href="{{ route('viewuserentries', ['iduser' => $entry->userid]) }}" class="label label-default">{{ $entry->username }}</a></p>
                                            <ul class="list-inline"><li><a href="#">

                                                        <?php
                                                        $dt = \Carbon\Carbon::parse($entry->created_at);
                                                        echo \Carbon\Carbon::now()->diffForHumans($dt);
                                                        ?>
                                        </div>

                                    </div>
                                    <br><br>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                            {!! $entries->appends(Request::input())->render() !!}

                        @if(count($entries)==0)
                                The user has not created entries
                            @endif
                    </div>
                </div>

            </div><!--/col-12-->
            <div class="col-sm-4 tweets">

            @if(count($tweets)>0)
               <h2> Tweets of @<?php echo$userdata->twitteruser ?></h2>
                    <div class="tweets-container">
                    @foreach($tweets as $tweet)
                        <?php if($tweet['visible'] ==true){ ?>
                        <div class="tweet-container" data-idtweet="{{ $tweet['id_str'] }}" data-date="{{ $tweet['created_at'] }}">
                            <p>
                            {!! \Thujohn\Twitter\Facades\Twitter::linkify($tweet['text']) !!}
                            </p>
                            <br>
                            {{ \Thujohn\Twitter\Facades\Twitter::ago($tweet['created_at']) }}
                            @if (!Auth::guest()  && !Auth::guest() && $authcheck!=null)
                            <button class="btn-sm btn-info btn-hide-tweet" >Hide Tweet</button>
                            @endif
                        </div>
                        <?php }else{
                                ?>
                            <div class="tweet-container" data-idtweet="{{ $tweet['id_str'] }}">
                                <p>The Owner hide this tweet</p>
                                @if (!Auth::guest() && !Auth::guest() && $authcheck!=null )
                                <button class="btn-sm btn-info btn-hide-tweet" >Show Tweet</button>
                                @endif
                            </div>
                        <?php
                            } ?>
                    @endforeach
                </div>
                <div class="row">

                    <div class="btn-lg btn-success" id="btn-load-more" >Load More Tweets</div>


                    <div class="preloader"><img src='/assets/img/loading.gif' ></div>

                </div>
            @else
                The user no have a twitter account or the username is invalid
                @endif
            </div>
        </div>
    </div>
@endsection

@include('partials.template.tweet')
@section('scripts')

    <script>
        var auth=null;
        @if (!Auth::guest() && $authcheck!=null)
            auth=true;
        @endif
        var twitteruser='{{ $userdata->twitteruser }}';


    </script>
    <script src="/assets/scripts/userentries.min.js"></script>

    @endsection