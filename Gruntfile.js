/**
 * Created by cesarsuarez on 7/12/15.
 */

module.exports = function (grunt) {
    grunt.initConfig({
        responsive_images: {
            texturas: {
                options: {
                    engine: 'im',
                    sizes: [{
                        width: 71,
                        height: 71,
//                          suffix: '_large_2x',
                        filter: 'Gaussian',
                        quality: 30
                    }]
                },
                files: [{
                    expand: true,
                    src: ['**/*.{jpg,png}'],
                    cwd: 'assets/images/colecciones/',
                    dest: 'images/colecciones'
                }]
            }
        },
        clean: {
            css: ["public/assets/css/*/*.min.css", "public/assets/css/*.min.css"]
        },
        sass: {// Task
            blog:{
                options: {// Target options
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: 'resources/assets/sass/',
                    src: ['**/*.scss'],
                    dest: 'public/assets/css',
                    ext: '.css'
                }]
            }
        },
        cssmin: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'public/assets/css/',
                    src: ['*.css', '!*.min.css', '!*.css.map'],
                    dest: 'public/assets/css/',
                    ext: '.min.css'
                }]
            },
        },

        uglify: {
            options: {
                mangle: false,
                beautify: false
            },
            blog: {
                files: {
                    'public/assets/scripts/userentries.min.js': ['resources/assets/scripts/userentries.js'],
                }
            }
        },
        copy: {
            //Copy files from boostrap
            bootstrap: {
                //Files front fonts
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/bootstrap-sass/assets/fonts/bootstrap/*'],
                        dest: 'public/assets/fonts/bootstrap/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js'],
                        dest: 'public/assets/scripts/vendor/',
                        filter: 'isFile'
                    },

                    {
                        expand: true,
                        flatten: true,
                        src: ['bower_components/jquery/dist/jquery.min.js'],
                        dest: 'public/assets/scripts/vendor/',
                        filter: 'isFile'
                    },
                ]
            }

        },

        //This are for the scripts of the application
        watch: {
            blog: {
                files: ['resources/assets/scripts/*.js'],
                tasks: ['uglify:blog'],
                options: {
                    spawn: false,
                },
            },
            styles: {
                files: ['resources/assets/sass/**/*.scss'],
                // tasks: ['clean:css','sass:w2play', 'cssmin'],
                tasks: ['sass:blog', 'cssmin'],
                options: {
                    spawn: false,
                },
            },
        },

    });

    grunt.loadNpmTasks('grunt-responsive-images');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    // grunt.registerTask('minimizarCSS', ['clean:css', 'cssmin:tablemac', 'cssmin:ngDialog', 'cssmin:boostrap']);
    grunt.registerTask('vercss', ['watch:styles']);
    grunt.registerTask('verjs', ['watch:blog']);
    grunt.registerTask('compressjs', ['uglify:blog']);

    //This task for initial setup
    grunt.registerTask('boostrap-sass-composer', ['copy:bootstrap','sass:blog','cssmin:main']);




}